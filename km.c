#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

#define RANDNUM_W 521288629;
#define RANDNUM_Z 362436069;

unsigned int randum_w = RANDNUM_W;
unsigned int randum_z = RANDNUM_Z;

void srandnum(int seed) {
  unsigned int w, z;
  w = (seed * 104623) & 0xffffffff;
  randum_w = (w) ? w : RANDNUM_W;
  z = (seed * 48947) & 0xffffffff;
  randum_z = (z) ? z : RANDNUM_Z;
}

unsigned int randnum(void) {
  unsigned int u;
  randum_z = 36969 * (randum_z & 65535) + (randum_z >> 16);
  randum_w = 18000 * (randum_w & 65535) + (randum_w >> 16);
  u = (randum_z << 16) + randum_w;
  return (u);
}

typedef float* vector_t;

int npoints;
int dimension;
int ncentroids;
float mindistance;
int seed;
vector_t *data, *centroids;
int *map;
int *dirty;
int too_far;
int has_changed;
int rank, size;
double ppp;

float v_distance(vector_t a, vector_t b) {
  int i;
  float distance = 0;
  for (i = 0; i < dimension; i++)
    distance +=  pow(a[i] - b[i], 2);
  return sqrt(distance);
}

static void populate(void) {
  int i, j;
  float tmp;
  float distance;
  too_far = 0;
  int tmp_too_far;
  int *tmp_dirty = malloc(sizeof(int)*ncentroids);

  for (i = (int) (((double) rank) * ppp); i < (int) (((double) (rank+1)) * ppp); i++) {
    distance = v_distance(centroids[map[i]], data[i]);
    /* Look for closest cluster. */
    for (j = 0; j < ncentroids; j++) {
      /* Point is in this cluster. */
      if (j == map[i]) continue;
      tmp = v_distance(centroids[j], data[i]);
      if (tmp < distance) {
        map[i] = j;
        distance = tmp;
        dirty[j] = 1;
      }
    }
    /* Cluster is too far away. */
    if (distance > mindistance)
      too_far = 1;
  }

  MPI_Allreduce(&too_far, &tmp_too_far, 1, MPI_INT, MPI_LOR, MPI_COMM_WORLD);
  MPI_Allreduce(dirty, tmp_dirty, ncentroids, MPI_INT, MPI_LOR, MPI_COMM_WORLD);

  too_far = tmp_too_far;
  free(dirty);
  dirty = tmp_dirty;
}

static void compute_centroids(void) {
  int i, j, k;
  int population;
  int tmp_population;
  int tmp_has_changed;
  has_changed = 0;

  vector_t *tmp_centroids = malloc(sizeof(vector_t)*ncentroids);
  for (int i = 0; i < ncentroids; ++i) {
    tmp_centroids[i] = malloc(sizeof(float)*dimension);
  }

  /* Compute means. */
  for (i = 0; i < ncentroids; i++) {
    if (!dirty[i]) continue;
    memset(centroids[i], 0, sizeof(float) * dimension);
    /* Compute cluster's mean. */
    population = 0;
    for (j = (int) (((double) rank) * ppp); j < (int) (((double) (rank+1)) * ppp); j++) {
      if (map[j] != i) continue;
      for (k = 0; k < dimension; k++)
        centroids[i][k] += data[j][k];
      population++;
    }

    MPI_Allreduce(centroids[i], tmp_centroids[i], dimension, MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD);
    free(centroids[i]);
    centroids[i] = tmp_centroids[i];

    MPI_Allreduce(&population, &tmp_population, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    population = tmp_population;

    if (population > 1) {
      for (k = 0; k < dimension; k++)
        centroids[i][k] *= 1.0/population;
    }
    has_changed = 1;
  }
  memset(dirty, 0, ncentroids * sizeof(int));

  MPI_Allreduce(&has_changed, &tmp_has_changed, 1, MPI_INT, MPI_LOR, MPI_COMM_WORLD);
}

void kmeans(void) {
  int i, j, k;
  too_far = 0;
  has_changed = 0;

  if (!(map  = calloc(npoints, sizeof(int))))
    exit (1);
  if (!(dirty = malloc(ncentroids*sizeof(int))))
    exit (1);
  if (!(centroids = malloc(ncentroids*sizeof(vector_t))))
    exit (1);

  for (i = 0; i < ncentroids; i++)
    centroids[i] = malloc(sizeof(float) * dimension);
  // for (i = 0; i < npoints; i++)
  //   map[i] = -1;
  for (i = 0; i < ncentroids; i++) {
    dirty[i] = 1;
    j = randnum() % npoints;
    for (k = 0; k < dimension; k++)
      centroids[i][k] = data[j][k];
    //map[j] = i;
  }
  /* Map unmapped data points. */
  // for cmap[i] = randnum() % ncentroids;

  do { /* Cluster data. */
    populate();
    compute_centroids();
  } while (too_far && has_changed);

  for (i = 0; i < ncentroids; i++)
    free(centroids[i]);
  free(centroids);
  free(dirty);

  int *tmp_map = malloc(sizeof(int)*npoints);
  MPI_Allreduce(map, tmp_map, npoints, MPI_INT, MPI_BOR, MPI_COMM_WORLD);
  free(map);
  map = tmp_map;

  if (rank == 0) {
    for (i = 0; i < ncentroids; i++) {
      printf("\nPartition %d:\n", i);
      for (j = 0; j < npoints; j++)
        if(map[j] == i)
          printf("%d ", j);
    }
    printf("\n");
  }

  free(map);
}

int main(int argc, char **argv) {
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  int i, j, tmp;

  if (argc != 6) {
    printf("Usage: npoints dimension ncentroids mindistance seed\n");
    exit (1);
  }

  npoints = atoi(argv[1]);
  dimension = atoi(argv[2]);
  ncentroids = atoi(argv[3]);
  mindistance = atoi(argv[4]);
  seed = atoi(argv[5]);

  ppp = ((double) npoints) / ((double) size);

  srandnum(seed);

  if (!(data = malloc(npoints*sizeof(vector_t))))
    exit(1);

  for (i = 0; i < npoints; i++) {
    data[i] = malloc(sizeof(float) * dimension);
    for (j = 0; j < dimension; j++)
      data[i][j] = randnum() & 0xffff;
  }

  //printf("rank %d [%d, %d)\n", rank, (int) (((double) rank )* ppp), (int) (((double) (rank+1)) * ppp));
  kmeans();

  for (i = 0; i < npoints; i++)
    free(data[i]);
  free(data);

  MPI_Finalize();
  return (0);
}
